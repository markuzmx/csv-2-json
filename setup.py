from setuptools import setup

setup(name='csv2json',
      version='0.1',
      description='Data conversor from csv to json, to create the fixtures for django apps',
      url='http://gitlab.csn.uchile.cl/dpineda/csv2json',
      author='David Pineda Osorio',
      author_email='dpineda@csn.uchile.cl',
      license='MIT',
      packages=[],
      zip_safe=False)

from csv2json.funciones import read_file, add_list
from csv2json.action import CSV2JSON


if __name__ == "__main__":
    files = [
        ('organization.kindoforganization',
         'kindoforganization.csv', 'name',
         {"description": read_file}),
        ('organization.organizationinfo',
         'organization.csv',
         'name', {"kind": add_list}),
    ]
    kwargs = {
        'files': files,
        'origin': './csv',
        'destiny': './json'
    }
    CSV2JSON(**kwargs)

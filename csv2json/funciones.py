def read_file(file_path):
    with open(file_path, 'r') as reader:
        return reader.readlines.pop()


def add_list(elem_list):
    """
    Para referenciar a elementos de otros modelos
    """
    return [element_list,]
